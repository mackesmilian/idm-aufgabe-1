#
# Copyright Campus 02
# Author: Thomas Strutz, Lukas Zepf
#
import hashlib


def md5_example(to_encode):
    return hashlib.md5(to_encode).hexdigest()


if __name__ == '__main__':
    print("Please enter message: ")
    usr_input = input().encode(encoding='UTF-8')

    print(md5_example(usr_input), ' <= "', usr_input, '"', sep='')
