import hashlib


def sha_demo():
    clear_text = input("Bitte Text eingeben: ").encode(encoding="UTF-8")
    hash = hashlib.sha256(clear_text)
    print(hash.hexdigest())


sha_demo()
