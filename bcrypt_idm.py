import bcrypt

password = input("Password: ")
salt = bcrypt.gensalt()
hashed_password = bcrypt.hashpw(password.encode('utf-8'), salt)
print (hashed_password)
hash = input("Hash (if empty the just generated hash will be used): ")
if hash == "":
    hash = hashed_password
else:
    hash = hash.encode('utf-8')
password = input("Password: ")
if bcrypt.checkpw(password.encode('utf-8'), hash):
    print("Password OK")
else:
    print("Wrong password")
