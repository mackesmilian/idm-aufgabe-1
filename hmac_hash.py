def hmac(input):

    import hmac 
    import hashlib

    message = input 
    key = "e179017a-62b0-4996-8a38-e91aa9f1"

    byte_key = bytes(key, 'UTF-8')
    #converts key to byte

    msg_encoded = message.encode()
    # returns an utf-8 encoded version of the string. 

    h = hmac.new(byte_key, msg_encoded, hashlib.sha256)
    #hmac takes 3 params:
    # the key which can be changed,
    # the msg in utf 8
    # and a varying message digest algorithm
    # returns hashed massage

    return h



if __name__ == '__main__':

    import sys

    input = sys.argv[1] 
    #takes first programm argument
    output = hmac(input).hexdigest()
    print(output)

