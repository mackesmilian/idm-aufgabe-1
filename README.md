# IDM - Aufgabe 1 - https://gitlab.com/mackesmilian/idm-aufgabe-1



## Getting started

SHA256 - Mackes und Coean
BCRYPT - Phillip
HMAC - Vinzi - Mathias
MD5 - Thomas - Lukas

## MD5

message-digest algorithm
- accepts message of any length
- returns output with fixed length - 128 bit
- processes data in 512-bit strings, 16 words, of 32 bits each
- add padding - 512-bit blocks - adding padding to fill up rest of the space in the block
- has been deprecated (attacker could generate a collision in as little as 10 seconds on a 2.6 gigahertz Pentium 4 system)

https://upload.wikimedia.org/wikipedia/commons/a/a5/MD5_algorithm.svg

Eine MD5-Operation. MD5 besteht aus 64 Operationen dieses Typs, gruppiert in 4 Durchläufen mit jeweils 16 Operationen.
F ist eine nichtlineare Funktion, die im jeweiligen Durchlauf eingesetzt wird. Mi bezeichnet einen 32-Bit-Block des
Eingabestroms und Ki eine für jede Operation unterschiedliche 32-Bit-Konstante; left shifts bezeichnet die bitweise
Linksrotation um s Stellen, wobei s für jede Operation variiert. Addition bezeichnet die Addition modulo 232.


## SHA256

SHA-2 oder secure hash algorithm ist der Oberbegriff für mehrere SHA Hashalgorithmen, darunter
auch SHA-256. SHA-2 ist eine Weiterentwicklung von SHA-1.

### Grobe Funktionsweise:
Die Nachricht wird erweitert und dann in sechszehn-Wörter Blöcke aufgeteilt. Diese werden
iterativ bearbeitet und somit verschlüsselt. Bei SHA-256 beträgt die Schlüsselänge 32 bit

Eingesetzt wird SHA-256 zum Beispiel als Hashwert bei Downloads verwendet um die
Integrität der Datei sicherzustellen.
